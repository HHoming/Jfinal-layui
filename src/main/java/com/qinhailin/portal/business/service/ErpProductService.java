
package com.qinhailin.portal.business.service;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;
import com.qinhailin.portal.business.model.ErpProduct;
import com.qinhailin.common.base.service.BaseService;
import com.qinhailin.common.vo.Grid;

/**
 * 产品表
 * @author qinhailin
 * @date 2020-07-19
 */
public class ErpProductService extends BaseService {
	private ErpProduct dao = new ErpProduct().dao();

    @Override
   public Model<?> getDao(){
    	return dao;
   }

   public Grid page(int pageNumber, int pageSize,Record record) {
      Record rd = new Record();
      //rd.set("user_name like", record.getStr("searchName"));
      return queryForList(pageNumber, pageSize,rd);
   }
}

