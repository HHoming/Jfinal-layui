
package com.qinhailin.portal.business.service;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;
import com.qinhailin.portal.business.model.ErpCustomerOrder;
import com.qinhailin.common.base.service.BaseService;
import com.qinhailin.common.vo.Grid;

/**
 * 客户订单
 * @author qinhailin
 * @date 2020-07-20
 */
public class ErpCustomerOrderService extends BaseService {
	private ErpCustomerOrder dao = new ErpCustomerOrder().dao();

    @Override
   public Model<ErpCustomerOrder> getDao(){
    	return dao;
   }

   public Grid page(int pageNumber, int pageSize,Record record) {
      Record rd = new Record();
      //rd.set("user_name like", record.getStr("searchName"));
      return queryForList(pageNumber, pageSize,rd);
   }
}

