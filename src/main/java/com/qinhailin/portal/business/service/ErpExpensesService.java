
package com.qinhailin.portal.business.service;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;
import com.qinhailin.portal.business.model.ErpExpenses;
import com.qinhailin.common.base.service.BaseService;
import com.qinhailin.common.vo.Grid;

/**
 * 日常开支
 * @author qinhailin
 * @date 2020-07-20
 */
public class ErpExpensesService extends BaseService {
	private ErpExpenses dao = new ErpExpenses().dao();

    @Override
   public Model<ErpExpenses> getDao(){
    	return dao;
   }

   public Grid page(int pageNumber, int pageSize,Record record) {
      Record rd = new Record();
      //rd.set("user_name like", record.getStr("searchName"));
      return queryForList(pageNumber, pageSize,rd);
   }
}

