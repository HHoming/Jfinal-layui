
package com.qinhailin.portal.business.ctrl;

import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Record;
import com.qinhailin.common.base.BaseController;
import com.qinhailin.common.routes.ControllerBind;
import com.qinhailin.portal.business.model.ErpProduct;
import com.qinhailin.portal.business.service.ErpProductService;

import java.util.Date;

/**
 * 产品表
 * @author qinhailin
 * @date 2020-07-19
 */
@ControllerBind(path="/portal/business/erpProduct")
public class ErpProductController extends BaseController {

   @Inject
   ErpProductService service;

  	public void index(){
    	render("index.html");
  	}
    
   public void list() {
      Record record=getAllParamsToRecord();
      renderJson(service.page(getParaToInt("pageNumber", 1), getParaToInt("pageSize", 10),record));
	}
    
   public void add() {
    	render("add.html");
   }

   public void save() {
       ErpProduct entity = getBean(ErpProduct.class);
       entity.setId(createUUID())
               .setCreatedTime(new Date()).save();
       setAttr("erpProduct", entity);
       render("add.html");
   }

   public void edit() {
      setAttr("erpProduct", service.findById(getPara(0)));
      render("edit.html");
   }

   public void update() {
      ErpProduct entity=getBean(ErpProduct.class);
      entity.update();
      setAttr("erpProduct", entity);
      render("edit.html");
   }

   public void delete() {
      service.deleteByIds(getIds());
      renderJson(suc());
   }

}

