
package com.qinhailin.portal.business.ctrl;

import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Record;
import com.qinhailin.common.base.BaseController;
import com.qinhailin.common.routes.ControllerBind;
import com.qinhailin.portal.business.model.ErpCustomerOrder;
import com.qinhailin.portal.business.service.ErpCustomerOrderService;

import java.util.Date;

/**
 * 客户订单
 * @author qinhailin
 * @date 2020-07-20
 */
@ControllerBind(path="/portal/business/erpCustomerOrder")
public class ErpCustomerOrderController extends BaseController {

   @Inject
   ErpCustomerOrderService service;

  	public void index(){
    	render("index.html");
  	}
    
   public void list() {
      Record record=getAllParamsToRecord();
      renderJson(service.page(getParaToInt("pageNumber", 1), getParaToInt("pageSize", 10),record));
	}
    
   public void add() {
    	render("add.html");
   }

   public void save() {
    	ErpCustomerOrder entity=getBean(ErpCustomerOrder.class);
    	entity.setId(createUUID())
                .setCreatedTime(new Date()).save();
    	setAttr("erpCustomerOrder", entity);
    	render("add.html");
   }

   public void edit() {
      setAttr("erpCustomerOrder", service.findById(getPara(0)));
      render("edit.html");
   }

   public void update() {
      ErpCustomerOrder entity=getBean(ErpCustomerOrder.class);
      entity.update();
      setAttr("erpCustomerOrder", entity);
      render("edit.html");
   }

   public void delete() {
      service.deleteByIds(getIds());
      renderJson(suc());
   }

}

