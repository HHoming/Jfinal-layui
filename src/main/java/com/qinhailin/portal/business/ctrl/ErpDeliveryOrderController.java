
package com.qinhailin.portal.business.ctrl;

import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Record;

import com.qinhailin.common.base.BaseController;
import com.qinhailin.common.routes.ControllerBind;
import com.qinhailin.portal.business.model.ErpDeliveryOrder;
import com.qinhailin.portal.business.service.ErpDeliveryOrderService;

import java.util.Date;

/**
 * 出货订单
 * @author qinhailin
 * @date 2020-07-20
 */
@ControllerBind(path="/portal/business/erpDeliveryOrder")
public class ErpDeliveryOrderController extends BaseController {

   @Inject
   ErpDeliveryOrderService service;

  	public void index(){
    	render("index.html");
  	}
    
   public void list() {
      Record record=getAllParamsToRecord();
      renderJson(service.page(getParaToInt("pageNumber", 1), getParaToInt("pageSize", 10),record));
	}
    
   public void add() {
    	render("add.html");
   }

   public void save() {
    	ErpDeliveryOrder entity=getBean(ErpDeliveryOrder.class);
    	entity.setId(createUUID())
                .setCreatedTime(new Date()).save();
    	setAttr("erpDeliveryOrder", entity);
    	render("add.html");
   }

   public void edit() {
      setAttr("erpDeliveryOrder", service.findById(getPara(0)));
      render("edit.html");
   }

   public void update() {
      ErpDeliveryOrder entity=getBean(ErpDeliveryOrder.class);
      entity.update();
      setAttr("erpDeliveryOrder", entity);
      render("edit.html");
   }

   public void delete() {
      service.deleteByIds(getIds());
      renderJson(suc());
   }

}

