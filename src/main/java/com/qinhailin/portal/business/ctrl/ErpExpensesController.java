
package com.qinhailin.portal.business.ctrl;

import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Record;

import com.qinhailin.common.base.BaseController;
import com.qinhailin.common.routes.ControllerBind;
import com.qinhailin.portal.business.model.ErpExpenses;
import com.qinhailin.portal.business.service.ErpExpensesService;

import java.util.Date;

/**
 * 日常开支
 * @author qinhailin
 * @date 2020-07-20
 */
@ControllerBind(path="/portal/business/erpExpenses")
public class ErpExpensesController extends BaseController {

   @Inject
   ErpExpensesService service;

  	public void index(){
    	render("index.html");
  	}
    
   public void list() {
      Record record=getAllParamsToRecord();
      renderJson(service.page(getParaToInt("pageNumber", 1), getParaToInt("pageSize", 10),record));
	}
    
   public void add() {
    	render("add.html");
   }

   public void save() {
    	ErpExpenses entity=getBean(ErpExpenses.class);
    	entity.setId(createUUID())
                .setCreatedTime(new Date()).save();
    	setAttr("erpExpenses", entity);
    	render("add.html");
   }

   public void edit() {
      setAttr("erpExpenses", service.findById(getPara(0)));
      render("edit.html");
   }

   public void update() {
      ErpExpenses entity=getBean(ErpExpenses.class);
      entity.update();
      setAttr("erpExpenses", entity);
      render("edit.html");
   }

   public void delete() {
      service.deleteByIds(getIds());
      renderJson(suc());
   }

}

