package com.qinhailin.portal.business.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseErpExpenses<M extends BaseErpExpenses<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	/**
	 * 名称
	 */
	public M setName(java.lang.String name) {
		set("name", name);
		return (M)this;
	}
	
	/**
	 * 名称
	 */
	public java.lang.String getName() {
		return getStr("name");
	}

	/**
	 * 数量
	 */
	public M setCount(java.lang.Integer count) {
		set("count", count);
		return (M)this;
	}
	
	/**
	 * 数量
	 */
	public java.lang.Integer getCount() {
		return getInt("count");
	}

	/**
	 * 金额
	 */
	public M setAmount(java.math.BigDecimal amount) {
		set("amount", amount);
		return (M)this;
	}
	
	/**
	 * 金额
	 */
	public java.math.BigDecimal getAmount() {
		return get("amount");
	}

	/**
	 * 创建时间
	 */
	public M setCreatedTime(java.util.Date createdTime) {
		set("created_time", createdTime);
		return (M)this;
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreatedTime() {
		return get("created_time");
	}

}
